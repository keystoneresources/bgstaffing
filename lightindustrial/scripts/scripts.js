if(typeof console==="undefined"){
console={log:function(){
}};
}
var YDOM=YAHOO.util.Dom;
var YEVENT=YAHOO.util.Event;
var YELEMENT=YAHOO.util.Element;
var YANIM=YAHOO.util.Anim;
var YCALENDAR=YAHOO.widget.Calendar;
var YSLIDER=YAHOO.widget.Slider;
YSLIDER.prototype.verifyOffset=function(_1){
var _2=YAHOO.util.Dom.getXY(this.getEl());
if(_2){
if(isNaN(this.baselinePos[0])){
this.setThumbCenterPoint();
this.thumb.startOffset=this.thumb.getOffsetFromParent(_2);
}
if(_2[0]!=this.baselinePos[0]||_2[1]!=this.baselinePos[1]){
this.thumb.resetConstraints();
this.baselinePos=_2;
return false;
}
}
return true;
};
function FSForm(id){
this.id=id;
this.scriptRequestCounter=1;
this.page=1;
this.lastPage=1;
this.checks=[];
this.logicFields=[];
this.calculations=[];
this.calcFields=[];
this.calcFieldDefaults={};
this.init=function(){
if(this.lastPage>1){
var _4=this;
YEVENT.addListener("fsForm"+this.id,"keypress",function(e){
if(e.keyCode!=13){
return true;
}
if(document.activeElement){
if(document.activeElement.tagName=="TEXTAREA"){
return true;
}
_4.updateCalculations(document.activeElement.id.substring(5));
}
if(_4.page==_4.lastPage){
_4.submitForm();
}else{
_4.nextPage(_4.page);
}
YAHOO.util.Event.preventDefault(e);
YAHOO.util.Event.stopEvent(e);
return false;
});
}
for(var i=0;i<this.calcFields.length;i++){
var id=this.calcFields[i];
var _8=this.getFieldsByName("field"+id);
for(var j=0;j<_8.length;j++){
var _a=_8[j];
var _b=_a.type.toLowerCase();
var _c=_b=="radio"||_b=="checkbox"?"click":"change";
YEVENT.addListener(_a,_c,(function(id){
return function(){
this.updateCalculations(id);
};
})(id),this,true);
}
var _e=YDOM.get("field"+id+"_othervalue");
if(_e){
YEVENT.addListener(_e,"change",(function(id,_10){
return function(){
YDOM.get("field"+id+"_other").checked=_10.value!="";
this.updateCalculations(id);
};
})(id,_e),this,true);
}
}
for(var i=0;i<this.logicFields.length;i++){
var id=this.logicFields[i];
var _8=this.getFieldsByName("field"+id);
for(var j=0;j<_8.length;j++){
var _a=_8[j];
var _b=_a.type.toLowerCase();
var _c=_b=="radio"||_b=="checkbox"?"click":"change";
YEVENT.addListener(_a,_c,(function(id){
return function(){
this.checkLogic(id);
};
})(id),this,true);
}
this.checkLogic(id);
}
var _8=YDOM.getElementsByClassName("fsOtherField","input");
for(var i=0;i<_8.length;i++){
var _a=_8[i];
YEVENT.addListener(_a,"change",function(e){
var _13=YEVENT.getTarget(e);
var id=_13.id.split("_");
YDOM.get(id[0]+"_other").checked=YDOM.get(_13).value!="";
},this,true);
}
var _8=YDOM.getElementsByClassName("fsField");
for(var i=0;i<_8.length;i++){
var _a=_8[i];
YEVENT.addListener(_a,"focus",function(e){
var _16=YEVENT.getTarget(e);
this.focus(_16,true);
},this,true);
YEVENT.addListener(_a,"blur",function(e){
var _18=YEVENT.getTarget(e);
this.focus(_18,false);
},this,true);
}
var els=YDOM.getElementsByClassName("fsCallout","div");
for(var i=0;i<els.length;i++){
var el=els[i];
YDOM.setStyle(el,"opacity",0);
FSUtil.hide(el);
}
for(var i=0;i<this.calculations.length;i++){
var _1b=this.calculations[i];
this.evalCalculation(_1b);
}
var _1c=YDOM.getElementsByClassName("fsCalendar","div");
for(var i=0;i<_1c.length;i++){
var div=_1c[i];
var id=div.id.match(/(\d+)/);
id=id[1];
var _1e=YDOM.get("field"+id+"Y").options;
var _1f=parseInt(_1e[1].value,10);
var _20=parseInt(_1e[_1e.length-1].value,10);
var _21=new Date().getFullYear();
if(_1f<100){
_1f+=_1f>_21-2000?1900:2000;
}
if(_20<100){
_20+=2000;
}
var _22=new YCALENDAR(div.id,div.id,{mindate:"1/1/"+_1f,maxdate:"12/31/"+_20,close:true});
_22.render();
YEVENT.addListener(div.id+"Link","click",_22.show,_22,true);
_22.beforeShowEvent.subscribe(this.calendarShow,_22,true);
_22.selectEvent.subscribe(this.calendarSelect,_22,true);
}
var _23=YDOM.getElementsByClassName("fsTextAreaMaxLength","textarea");
for(var i=0;i<_23.length;i++){
var _24=_23[i];
var id=_24.id.match(/(\d+)/);
id=id[1];
var _25=YDOM.get("fsCounter"+id);
var _26=parseInt(_25.innerHTML);
if(_26>0){
YEVENT.addListener(_24,"keyup",(function(id,_28){
return function(){
this.textareaCharLimiter(id,_28);
};
})(id,_26),this,true);
YDOM.setStyle(_24.id,"paddingBottom","24px");
_25.innerHTML="";
FSUtil.show(_25);
}
}
var _29=YDOM.getElementsByClassName("fsMatrixOnePerColumn","table");
for(var i=0;i<_29.length;i++){
var _2a=_29[i].getElementsByTagName("input");
for(var j=0;j<_2a.length;j++){
var _b=_2a[j].type.toLowerCase();
if(_b=="radio"||_b=="checkbox"){
YEVENT.addListener(_2a[j],"click",(function(id){
return function(){
this.checkMatrixOnePerColumn(id);
};
})(_2a[j].id),this,true);
}
}
}
var _2c=YDOM.getElementsByClassName("fsSlider","input");
for(var i=0;i<_2c.length;i++){
var _2d=this.getNumberProperties(_2c[i]);
if(!isNaN(_2d.min)&&!isNaN(_2d.max)){
var _2e=YSLIDER.getHorizSlider(_2c[i].id+"-sliderbg",_2c[i].id+"-sliderthumb",0,100);
_2e._fsobj=this;
_2e._fsnumber=_2d;
_2e._fsfield=_2c[i];
_2e._fsshow=YDOM.get(_2c[i].id+"-slidervalue");
var _2f=_2c[i].value!=""?parseFloat(_2c[i].value):_2d.min;
if(isNaN(_2f)){
_2f=_2d.min;
}
if(!/msie/i.test(navigator.userAgent)||/opera/i.test(navigator.userAgent)){
var _30=Math.round((_2f-_2d.min)/(_2d.max-_2d.min)*100);
_2e.setValue(_30,false,true,true);
}
if(!isNaN(_2d.decimals)){
_2f=_2f.toFixed(_2d.decimals);
}
_2c[i].value=_2f;
_2e._fsshow.innerHTML=_2f;
_2e.subscribe("change",function(_31){
var _32=((_31/100)*(this._fsnumber.max-this._fsnumber.min))+this._fsnumber.min;
_32=isNaN(this._fsnumber.decimals)?Math.round(_32):_32.toFixed(this._fsnumber.decimals);
if(_32==-0){
_32=0;
}
this._fsfield.value=_32;
this._fsshow.innerHTML=_32;
var id=this._fsfield.id.match(/(\d+)/);
id=id[1];
if(FSUtil.arrayIndexOf(this._fsobj.calcFields,id)>=0){
this._fsobj.updateCalculations(id);
}
if(FSUtil.arrayIndexOf(this._fsobj.logicFields,id)>=0){
this._fsobj.checkLogic(id);
}
},_2e,true);
}
}
var _2a=[];
var _34=["fsFormatEmail","fsFormatPhoneUS","fsFormatPhoneUK","fsFormatPhoneAU","fsFormatPhoneXX","fsFormatZipUS","fsFormatZipCA","fsFormatZipUK","fsFormatZipAU","fsFormatNumber","fsFormatCreditCard"];
for(var _35=0;_35<_34.length;_35++){
_2a=_2a.concat(YDOM.getElementsByClassName(_34[_35],"input"));
}
for(var i=0;i<_2a.length;i++){
this.checkFormat(_2a[i]);
YEVENT.addListener(_2a[i],"change",(function(_36){
return function(){
this.checkFormat(_36);
};
})(_2a[i]),this,true);
}
this.updateProgress(1);
this.fitTableWidths(1);
if(!this.checkFreeLink()){
return;
}
};
this.getFieldContainer=function(_37){
var _38=_37;
while(_38&&_38.tagName.toLowerCase()!="body"){
if(YDOM.hasClass(_38,"fsFieldCell")){
return _38;
}
_38=_38.parentNode;
}
return;
};
this.focus=function(_39,_3a){
if(/MSIE 6/i.test(navigator.userAgent)){
return;
}
var _3b=this.getFieldContainer(_39);
if(!_3b){
return;
}
if(_3a){
YDOM.addClass(_3b,"fsFieldFocused");
this.showCallout(_3b,true);
}else{
YDOM.removeClass(_3b,"fsFieldFocused");
this.showCallout(_3b,false);
}
};
this.showCallout=function(_3c,_3d){
var _3e=this.getFieldContainer(_3c);
var _3f=YDOM.getElementsByClassName("fsCallout","div",_3e);
if(!_3f.length){
return;
}
var _40=_3f[0];
if(_3d){
var _41=YDOM.getXY(_3c);
var _42=FSUtil.getHeight(_3c);
var _43=FSUtil.getWidth(_3c);
YDOM.setStyle(_40,"opacity",0);
YDOM.setStyle(_40,"top",(_41[1])+_42+"px");
YDOM.setStyle(_40,"left",(_41[0]+50)+"px");
YDOM.setStyle(_40,"marginTop","25px");
FSUtil.show(_40);
var _44=new YAHOO.util.Anim(_40,{marginTop:{to:0},opacity:{to:1}},0.5,YAHOO.util.Easing.easeOut);
_44.animate();
}else{
var _44=new YAHOO.util.Anim(_40,{opacity:{to:0}},0.5,YAHOO.util.Easing.easeOut);
_44.onComplete.subscribe(function(){
FSUtil.hide(_40);
});
_44.animate();
}
};
this.fadeCallout=function(_45){
var _46=15;
var _47=20;
var _48=YDOM.hasClass(_45,"fsCalloutShowing");
var _49=YDOM.getStyle(_45,"opacity");
var _4a=YDOM.getStyle(_45,"marginTop").split("px")[0];
var _4b=this;
if(_48){
_49+=(1/_46);
_4a-=(25/_46);
if(_49>=1){
_49=1;
}else{
setTimeout(function(){

_4b.fadeCallout(_45);
},_47);
}
if(_4a<=0){
_4a=0;
}
}else{
_49-=(1/_46);
if(_49<=0){
_49=0;
FSUtil.hide(_45);
}else{
setTimeout(function(){
_4b.fadeCallout(_45);
},_47);
}
}
YDOM.setStyle(_45,"opacity",_49);
YDOM.setStyle(_45,"margin-top",_4a+"px");
};
this.checkRequired=function(_4c){
this.clearError(_4c);
var _4d=false;
var _4e=[];
var _4f=YDOM.getElementsByClassName("fsField","","fsPage"+this.id+"-"+_4c);
for(var i=0;i<_4f.length;i++){
var _51=_4f[i];
if(this.fieldIsVisible(_51)&&FSUtil.arrayIndexOf(_4e,_51.id)==-1){
var _52=true;
if(YDOM.hasClass(_51,"fsRequired")){
var _53=_51.name.substr(0,_51.name.indexOf("-"));
var _54=document.getElementById("matrix-"+_53);
if(_54!=null){
var _55=_54.getElementsByTagName("input");
var _56=new Array(false);
for(var j=0;j<_55.length;j++){
hasRadio=false;
var _58=_55[j].type.toLowerCase();
if(_58=="radio"||_58=="checkbox"){
if(_58=="radio"){
hasRadio=true;
_52=this.checkValue(_51);
}
var col=parseInt(_55[j].id.substr(_55[j].id.length-1))-1;
if(_56[col]==null){
_56[col]=false;
}
if(_55[j].checked){
_56[col]=true;
}
}
}
if(YDOM.hasClass(_54,"fsMatrixOnePerColumn")){
_52=true;
for(var c=0;c<_56.length;c++){
if(_56[c]==false){
_52=false;
}
}
}else{
if(!hasRadio){
_52=false;
for(var c=0;c<_56.length;c++){
if(_56[c]==true){
_52=true;
break;
}
}
}
}
if(_52==false){
this.highlightField(_51,true);
}
}else{
_52=this.checkValue(_51);
}
if(!_52){
_4d=true;
if(YDOM.hasClass(_51,"fsFieldAddress")){
var id=_51.id.split("-");
id=id[0];
_4e.push(id+"-zip");
}
}
}
if(_52&&YDOM.hasClass(_51,"fsUpload")){
_52=this.checkUpload(_51);
if(!_52){
_4d=true;
}
}
if(_52){
_52=this.checkFormat(_51);
if(!_52){
_4d=true;
}
}
}
}
if(_4d){
this.showError(YDOM.get("requiredFieldsError")?YDOM.get("requiredFieldsError").innerHTML:"Please fill in a valid value for all required fields");
return false;
}
return true;
};
this.checkValue=function(_5c){
var bad=false;
switch(_5c.type.toLowerCase()){
case "text":
case "password":
case "textarea":
case "file":
case "email":
case "tel":
if(YDOM.hasClass(_5c,"fsFieldName")){
var id=_5c.id.split("-");
id=id[0];
bad=!YDOM.get(id+"-first").value.match(/\S/)||!YDOM.get(id+"-last").value.match(/\S/);
}else{
if(YDOM.hasClass(_5c,"fsFieldAddress")){
var id=_5c.id.split("-");
id=id[0];
bad=!YDOM.get(id+"-address").value.match(/\S/)||!YDOM.get(id+"-city").value.match(/\S/)||!YDOM.get(id+"-zip").value.match(/\S/);
if(!bad){
var _5f=YDOM.get(id+"-state");
if(_5f.type.toLowerCase()=="select-one"){
bad=!_5f.options[_5f.selectedIndex].value.match(/\S/);
}else{
bad=!_5f.value.match(/\S/);
}
}
if(!bad){
var _60=YDOM.get(id+"-country");
if(_60&&!_60.options[_60.selectedIndex].value.match(/\S/)){
bad=true;
}
}
}else{
bad=!_5c.value.match(/\S/);
}
}
break;
case "select-one":
bad=!_5c.options[_5c.selectedIndex].value.match(/\S/);
break;
case "select-multiple":
bad=true;
var _61=_5c.options;
for(var j=0;j<_61.length;j++){
if(_61[j].selected&&_61[j].value.match(/\S/)){
bad=false;
}
}
break;
case "radio":
case "checkbox":
bad=true;
var _63=document.getElementsByName(_5c.name);
for(var j=0;j<_63.length;j++){
if(_63[j].checked){
bad=false;
}
}
break;
}
if(bad){
this.highlightField(_5c,true);
}
return !bad;
};
this.checkFormat=function(_64){
var _65=false;
if(_64.value!=""){
if(YDOM.hasClass(_64,"fsFormatEmail")){
_65=true;
if(!_64.value.match(/^\s*\S+\@[\w\-\.]+\.\w+\s*$/)){
this.highlightField(_64,true);
return false;
}
}else{
if(YDOM.hasClass(_64,"fsFormatPhoneUS")||YDOM.hasClass(_64,"fsFormatPhoneUK")||YDOM.hasClass(_64,"fsFormatPhoneAU")){
_65=true;
var val=_64.value.toLowerCase().replace(/[^\dx]/g,"");
var ext="";
if(val.indexOf("x")>=0){
var _68=val.split("x");
val=_68[0];
ext=_68[1];
}
if(val.charAt(0)=="1"){
val=val.substr(1,val.length-1);
}
if(YDOM.hasClass(_64,"fsFormatPhoneUS")){
if(val.length!=10){
this.highlightField(_64,true);
return false;
}
_64.value="("+val.substr(0,3)+") "+val.substr(3,3)+"-"+val.substr(6,4);
}else{
if(YDOM.hasClass(_64,"fsFormatPhoneUK")){
if(val.substr(0,2)=="44"){
val=val.substr(2,val.length-2);
if(val.charAt(0)!="0"){
val="0"+val;
}
}
if(val.charAt(0)!="0"||(val.length!=10&&val.length!=11)){
this.highlightField(_64,true);
return false;
}
if((val.charAt(1)=="1"&&(val.charAt(2)=="1"||val.charAt(3)=="1"))||(val.charAt(1)=="8")){
_64.value=val.substr(0,4)+" "+val.substr(4,3)+" "+val.substr(7,val.length-7);
}else{
if(val.charAt(1)=="2"||val.charAt(1)=="3"||val.charAt(1)=="5"){
_64.value=val.substr(0,3)+" "+val.substr(3,4)+" "+val.substr(7,val.length-7);
}else{
_64.value=val.substr(0,5)+" "+val.substr(5,val.length-5);
}
}
}else{
if(YDOM.hasClass(_64,"fsFormatPhoneAU")){
if(val.substr(0,2)=="61"){
val=val.substr(2,val.length-2);
if(val.charAt(0)!="0"){
val="0"+val;
}
}
if(val.charAt(0)!="0"||val.length!=10){
this.highlightField(_64,true);
return false;
}
_64.value="("+val.substr(0,2)+") "+val.substr(2,4)+" "+val.substr(6,4);
}
}
}
if(ext.length){
_64.value+=" x"+ext;
}
}else{
if(YDOM.hasClass(_64,"fsFormatPhoneXX")){
_65=true;
if(!/\d{3,}/.test(_64.value)){
this.highlightField(_64,true);
return false;
}
}else{
if(YDOM.hasClass(_64,"fsFormatZipUS")){
_65=true;
var val=_64.value.replace(/^\s+/,"").replace(/\s+$/,"");
if(!val.match(/^\d{5}(?:\-\d{4})?$/)){
this.highlightField(_64,true);
return false;
}
_64.value=val;
}else{
if(YDOM.hasClass(_64,"fsFormatZipCA")){
_65=true;
var val=_64.value.replace(/^\s+/,"").replace(/\s+$/,"").replace(/\s{2,}/," ").toUpperCase();
if(val.length==6&&!val.match(/\s/)){
val=val.substr(0,3)+" "+val.substr(3,3);
}
if(!val.match(/^[A-Z]\d[A-Z] \d[A-Z]\d$/)){
this.highlightField(_64,true);
return false;
}
_64.value=val;
}else{
if(YDOM.hasClass(_64,"fsFormatZipUK")){
_65=true;
var val=_64.value.replace(/^\s+/,"").replace(/\s+$/,"").replace(/\s{2,}/," ").toUpperCase();
if(!val.match(/\s/)){
val=val.substr(0,val.length-3)+" "+val.substr(val.length-3,3);
}
if(!val.match(/^[A-Z]{1,2}[0-9R][0-9A-Z]? [0-9][ABD-HJLNP-UW-Z]{2}$/)){
this.highlightField(_64,true);
return false;
}
_64.value=val;
}else{
if(YDOM.hasClass(_64,"fsFormatZipAU")){
_65=true;
var val=_64.value.replace(/^\s+/,"").replace(/\s+$/,"").toUpperCase();
if(!val.match(/^\d{4}$/)){
this.highlightField(_64,true);
return false;
}
_64.value=val;
}else{
if(YDOM.hasClass(_64,"fsFormatNumber")){
_65=true;
var val=parseFloat(_64.value.replace(/[^\d\.\-]/g,""));
if(isNaN(val)){
this.highlightField(_64,true);
return false;
}
var _69=this.getNumberProperties(_64);
if(!isNaN(_69.min)&&val<_69.min){
this.highlightField(_64,true);
return false;
}
if(!isNaN(_69.max)&&val>_69.max){
this.highlightField(_64,true);
return false;
}
if(!isNaN(_69.decimals)){
val=val.toFixed(_69.decimals);
}
_64.value=val;
}else{
if(YDOM.hasClass(_64,"fsFormatCreditCard")){
_65=true;
var val=_64.value.replace(/\D/g,"");
var _6a=0;
var _6b=1;
for(var i=val.length-1;i>=0;i--){
var _6d=parseInt(val.charAt(i))*_6b;
_6a+=(_6d>9)?_6d-9:_6d;
_6b=_6b==1?2:1;
}
if(_6a%10!=0){
this.highlightField(_64,true);
return false;
}
if(val.match(/^4/)){
if(!YDOM.hasClass(_64,"fsFormatCreditCardVisa")||(val.length!=13&&val.length!=16)){
this.highlightField(_64,true);
return false;
}
}else{
if(val.match(/^(?:51|52|53|54|55)/)){
if(!YDOM.hasClass(_64,"fsFormatCreditCardMasterCard")||val.length!=16){
this.highlightField(_64,true);
return false;
}
}else{
if(val.match(/^(?:6011|622|64|65)/)){
if(!YDOM.hasClass(_64,"fsFormatCreditCardDiscover")||val.length!=16){
this.highlightField(_64,true);
return false;
}
}else{
if(val.match(/^(?:34|37)/)){
if(!YDOM.hasClass(_64,"fsFormatCreditCardAmex")||val.length!=15){
this.highlightField(_64,true);
return false;
}
}else{
if(val.match(/^(?:300|301|302|303|304|305|36|54|55)/)){
if(!YDOM.hasClass(_64,"fsFormatCreditCardDiners")||(val.length!=14&&val.length!=16)){
this.highlightField(_64,true);
return false;
}
}else{
if(val.match(/^35/)){
if(!YDOM.hasClass(_64,"fsFormatCreditCardJCB")||val.length!=16){
this.highlightField(_64,true);
return false;
}
}else{
this.highlightField(_64,true);
return false;
}
}
}
}
}
}
_64.value=val;
}
}
}
}
}
}
}
}
}
}
if(_65){
this.highlightField(_64,false);
}
return true;
};
this.checkUpload=function(_6e){
var _6f=true;
var _70=[];
var _71=_6e.className.split(/\s+/);
for(var j=0;j<_71.length;j++){
var _73=_71[j];
if(/^uploadTypes-/.test(_73)){
var m=_73.split("-");
_70=m[1].split(",");
}
}
for(var j=0;j<_70.length;j++){
_70[j]=_70[j].toLowerCase();
}
if(FSUtil.arrayIndexOf(_70,"*")<0&&_6e&&_6e.value!=""&&this.fieldIsVisible(_6e)){
var ext=_6e.value.match(/\.(\w+)$/);
_6f=ext&&FSUtil.arrayIndexOf(_70,ext[1].toLowerCase())>=0?true:false;
if(!_6f){
this.highlightField(_6e,true);
var msg=YDOM.get("fileTypeAlert")?YDOM.get("fileTypeAlert").innerHTML:"You must upload one of the following file types for the selected field:";
alert(msg+_70.join(", "));
}
}
return _6f;
};
this.showError=function(_77){
var _78=document.createElement("div");
_78.id="fsError"+this.id;
_78.className="fsError";
_78.innerHTML=_77;
YDOM.insertBefore(_78,"fsForm"+this.id);
FSUtil.scrollTo("fsError"+this.id);
};
this.clearError=function(_79){
var _7a=YDOM.getElementsByClassName("fsRequired","","fsPage"+this.id+"-"+_79);
for(var i=0;i<_7a.length;i++){
this.highlightField(_7a[i],0);
}
var _7a=YDOM.getElementsByClassName("fsUpload","input","fsPage"+this.id+"-"+_79);
for(var i=0;i<_7a.length;i++){
this.highlightField(_7a[i],0);
}
var _7c=YDOM.get("fsError"+this.id);
if(_7c){
_7c.parentNode.removeChild(_7c);
}
};
this.highlightField=function(_7d,on){
var _7f=this.getFieldContainer(_7d);
if(on){
YDOM.addClass(_7f,"fsValidationError");
}else{
YDOM.removeClass(_7f,"fsValidationError");
}
};
this.checkSelected=function(_80,_81){
var _82=false;
var _83=document.getElementsByName(_80);
if(!_83.length){
_83=document.getElementsByName(_80+"[]");
}
for(var i=0;i<_83.length;i++){
var _85=_83[i];
if(_85.type=="checkbox"||_85.type=="radio"){
if(_85.checked&&_85.value==_81){
_82=true;
}
}else{
if(_85.type=="select-one"){
_82=_85.value==_81;
}else{
if(_85.type=="select-multiple"){
var _86=_85.options;
for(var j=0;j<_86.length;j++){
var _88=_86[j];
if(_88.selected&&_88.value==_81){
_82=true;
}
}
}
}
}
}
return _82;
};
this.checkLogic=function(id){
for(var i=0;i<this.checks.length;i++){
var _8b=this.checks[i];
if(FSUtil.arrayIndexOf(_8b.fields,id)>=0){
var _8c=_8b.bool=="AND"?true:false;
for(var j=0;j<_8b.checks.length;j++){
var _8e=_8b.checks[j];
var _8f=false;
if(_8e.condition=="gt"){
_8f=Number(YDOM.get("field"+_8e.field).value)>Number(_8e.option);
}else{
if(_8e.condition=="lt"){
_8f=Number(YDOM.get("field"+_8e.field).value)<Number(_8e.option);
}else{
_8f=this.checkSelected("field"+_8e.field,_8e.option);
if(_8e.condition=="!="){
_8f=!_8f;
}
}
}
if(_8b.bool=="AND"){
_8c=_8c?_8f:false;
}else{
_8c=_8c?true:_8f;
}
}
var _90=YDOM.get("fsCell"+_8b.target);
if(YDOM.hasClass(_90,"fsSectionCell")){
_90=YDOM.get("fsSection"+_8b.target);
}
if(_8c){
if(_8b.action=="Show"){
this.showFields(_90);
}else{
this.hideFields(_90);
}
}else{
if(_8b.action=="Show"){
this.hideFields(_90);
}else{
this.showFields(_90);
}
}
}
}
};
this.showFields=function(_91){
var _92=["input","textarea","select"];
for(var i=0;i<_92.length;i++){
var _94=_91.getElementsByTagName(_92[i]);
for(var j=0;j<_94.length;j++){
var _96=_94[j];
if(_96.type!="file"){
_96.disabled=false;
}
}
}
if(_91.tagName.toLowerCase()=="table"){
if(!FSUtil.visible(_91)){
FSUtil.show(_91);
this.updateTablePositionClasses(_91);
}
}else{
YDOM.removeClass(_91,"fsHiddenCell");
FSUtil.show(YDOM.getAncestorByTagName(_91,"tr"));
var _97=YDOM.getAncestorByTagName(_91,"table");
if(!FSUtil.visible(_97)){
FSUtil.show(_97);
this.updateTablePositionClasses(_97);
}
var _98=YDOM.getElementsByClassName("fsMatrix","table",_91);
for(var _99=0;_99<_98.length;_99++){
var _9a=_98[_99].getElementsByTagName("td");
for(var _9b=0;_9b<_9a.length;_9b++){
YDOM.removeClass(_9a[_9b],"fsHiddenCell");
}
}
}
};
this.hideFields=function(_9c){
if(_9c.tagName.toLowerCase()=="table"){
if(FSUtil.visible(_9c)){
FSUtil.hide(_9c);
this.updateTablePositionClasses(_9c);
}
}else{
YDOM.addClass(_9c,"fsHiddenCell");
var _9d=YDOM.getAncestorByTagName(_9c,"tr");
var _9e=YDOM.getElementsByClassName("fsFieldCell","td",_9d);
var _9f=false;
if(_9e.length==1){
_9f=true;
}else{
var _a0=YDOM.getElementsByClassName("fsHiddenCell","td",_9d);
if(_a0.length==_9e.length){
_9f=true;
}
}
if(_9f){
FSUtil.hide(_9d);
var _a1=YDOM.getAncestorByTagName(_9c,"table");
var _a2=YDOM.getElementsByClassName("fsFieldRow","tr",_a1);
var _a3=false;
for(var i=0;i<_a2.length;i++){
if(FSUtil.visible(_a2[i])){
_a3=true;
break;
}
}
if(!_a3&&FSUtil.visible(_a1)){
FSUtil.hide(_a1);
this.updateTablePositionClasses(_a1);
}
}
}
var _a5=["input","textarea","select"];
for(var i=0;i<_a5.length;i++){
var _a6=_9c.getElementsByTagName(_a5[i]);
for(var j=0;j<_a6.length;j++){
var _a8=_a6[j];
if(_a8.type!="file"){
_a8.disabled=true;
}
}
}
};
this.updateTablePositionClasses=function(_a9){
var _aa=YDOM.getAncestorByTagName(_a9,"div");
if(!YDOM.hasClass(_aa,"fsPage")){
return;
}
var _ab=YDOM.getElementsByClassName("fsSection","table",_aa);
var _ac=-1;
var _ad=-1;
for(var i=0;i<_ab.length;i++){
if(FSUtil.visible(_ab[i])){
if(_ac<0){
_ac=i;
YDOM.addClass(_ab[i],"fsFirstSection");
YDOM.removeClass(_ab[i],"fsMiddleSection");
YDOM.removeClass(_ab[i],"fsLastSection");
}else{
YDOM.addClass(_ab[i],"fsMiddleSection");
YDOM.removeClass(_ab[i],"fsFirstSection");
YDOM.removeClass(_ab[i],"fsLastSection");
}
YDOM.removeClass(_ab[_ad],"fsSingleSection");
_ad=i;
}
}
if(_ad>=0){
YDOM.removeClass(_ab[_ad],"fsMiddleSection");
if(_ad==_ac){
YDOM.addClass(_ab[_ad],"fsSingleSection");
YDOM.removeClass(_ab[_ad],"fsFirstSection");
YDOM.removeClass(_ab[_ad],"fsLastSection");
}else{
YDOM.addClass(_ab[_ad],"fsLastSection");
YDOM.removeClass(_ab[_ad],"fsFirstSection");
}
}
};
this.getCalculation=function(id){
for(var i=0;i<this.calculations.length;i++){
var _b1=this.calculations[i];
if(FSUtil.arrayIndexOf(_b1.fields,id)>=0){
return _b1;
}
}
return null;
};
this.getCalculationByTarget=function(_b2){
for(var i=0;i<this.calculations.length;i++){
var _b4=this.calculations[i];
if(_b4.target==_b2){
return _b4;
}
}
return null;
};
this.updateCalculations=function(id){
for(var i=0;i<this.calculations.length;i++){
var _b7=this.calculations[i];
if(FSUtil.arrayIndexOf(_b7.fields,id)>=0){
this.evalCalculation(_b7);
}
}
};
this.evalCalculation=function(_b8){
var _b9=_b8.equation;
var _ba="";
for(var i=0;i<_b8.fields.length;i++){
var id=_b8.fields[i];
var _bd=new RegExp("\\["+id+"\\]","g");
var val=0;
var _bf=this.getFieldsByName("field"+id);
var _c0=_bf.length;
for(var j=0;j<_c0;j++){
var _c2=_bf[j];
var _c3;
switch(_c2.type.toLowerCase()){
case "radio":
case "checkbox":
if(_c2.value=="Other"&&YDOM.get(_c2.id+"value")){
_c3=YDOM.get(_c2.id+"value").value;
}else{
_c3=_c2.value;
}
var v=this.getNumber(_c3);
if(_c2.checked&&!isNaN(v)){
val+=v;
}
break;
case "select-multiple":
var _c5=_c2.options;
for(var k=0;k<_c5.length;k++){
var v=this.getNumber(_c5[k].value);
if(_c5[k].selected&&!isNaN(v)){
_c3=_c5[k].value;
val+=v;
}
}
break;
default:
_c3=YDOM.get(_c2).value;
var v=this.getNumber(YDOM.get(_c2).value);
if(!isNaN(v)){
val=v;
}
}
if(_c3&&_c3.indexOf("$")!=-1){
_ba="$";
}
}
_b9=_b9.replace(_bd,val);
}
var _c7=0;
try{
_c7=eval(_b9);
}
catch(e){
}
var _c2=YDOM.get("field"+_b8.target);
if(YDOM.hasClass(_c2,"fsFormatNumber")){
_c2.value=_c7;
this.checkFormat(_c2);
}else{
_c2.value=_ba+_c7.toFixed(2);
}
if(_c2.type=="text"){
this.checkLogic(_b8.target);
}
this.updateCalculations(_b8.target);
};
this.getNumber=function(str){
if(!str){
return;
}
if(str.indexOf(" == ")!=-1){
var _c9=str.split(" == ");
str=_c9[1];
}
return parseFloat(str.replace(/[^\d\.\-]/g,""));
};
this.previousPage=function(_ca){
var _cb=YDOM.get("fsPage"+this.id+"-"+_ca);
if(!_cb){
return;
}
if(_ca<=1){
return;
}
var _cc=_ca-1;
while(!this.pageIsVisible(_cc)&&_cc>1){
_cc--;
}
var _cd=YDOM.get("fsPage"+this.id+"-"+_cc);
FSUtil.hide(_cb);
FSUtil.show(_cd);
this.updateProgress(_cc);
this.clearError(_ca);
FSUtil.hide("fsSubmit"+this.id);
FSUtil.scrollTo(_cd);
this.fitTableWidths(_cc);
this.page--;
};
this.nextPage=function(_ce){
var _cf=YDOM.get("fsPage"+this.id+"-"+_ce);
if(!_cf){
return;
}
if(_ce>=this.lastPage){
return;
}
if(this.checkRequired(_ce)){
var _d0=_ce+1;
while(!this.pageIsVisible(_d0)&&_d0<this.lastPage){
_d0++;
}
this.updateProgress(_d0);
var _d1=YDOM.get("fsPage"+this.id+"-"+_d0);
FSUtil.hide(_cf);
FSUtil.show(_d1);
if(_d0==this.lastPage){
FSUtil.show("fsSubmit"+this.id);
}
FSUtil.scrollTo(_d1);
this.fitTableWidths(_d0);
this.page++;
if(_ce+1==this.lastPage){
if(window["cp"+this.id]!=null){
window["cp"+this.id].parsePage();
}
}
}
};
this.fitTableWidths=function(_d2){
if(!/msie/i.test(navigator.userAgent)||/opera/i.test(navigator.userAgent)){
return;
}
var _d3="fsPage"+this.id+"-"+_d2;
var _d4=YDOM.getElementsByClassName("fsTable","table",_d3);
var max=0;
for(var i=0;i<_d4.length;i++){
var _d7=_d4[i].scrollWidth;
if(_d7>max){
max=_d7;
}
}
if(max){
YDOM.setStyle("fsForm"+this.id,"width",max+"px");
}
};
this.updateProgress=function(_d8){
if(!YDOM.get("fsProgress"+this.id+"-"+_d8)){
return;
}
var _d9=YDOM.getElementsByClassName("fsPage","div","fsForm"+this.id).length;
if(_d9<=1){
FSUtil.hide("fsProgress"+this.id+"-"+_d8);
return;
}
var _da=YDOM.get("fsProgressBarContainer"+this.id+"-"+_d8);
var _db=YDOM.get("fsProgressBar"+this.id+"-"+_d8);
var _dc=100;
var _dd=_d8/_d9;
if(_dd<0){
_dd=0;
}
if(_dd>1){
_dd=1;
}
var _de=(_dc*_dd)+"px";
YDOM.setStyle(_db,"width",_de);
};
this.pageIsVisible=function(_df){
var _e0=false;
var _e1=YDOM.getElementsByClassName("fsFieldCell","td","fsPage"+this.id+"-"+_df);
for(var i=0;i<_e1.length;i++){
var _e3=_e1[i];
if(FSUtil.visible(_e3)&&!YDOM.hasClass(_e3,"fsHiddenCell")){
var _e4=YDOM.getAncestorByClassName(_e3,"fsSection");
if(!_e4||(FSUtil.visible(_e4)&&!YDOM.hasClass(_e4,"fsHiddenCell"))){
_e0=true;
}
}
}
var _e5=YDOM.getElementsByClassName("fsSection","table","fsPage"+this.id+"-"+_df);
for(var i=0;i<_e5.length;i++){
var _e4=_e5[i];
if(FSUtil.visible(_e4)&&!YDOM.hasClass(_e4,"fsHiddenCell")){
_e0=true;
}
}
return _e0;
};
this.fieldIsVisible=function(_e6){
var _e7=_e6.parentNode;
while(_e7&&_e7.tagName.toLowerCase()!="body"&&!YDOM.hasClass(_e7,"fsFieldCell")){
_e7=_e7.parentNode;
}
var _e8=_e7&&_e7.tagName.toLowerCase()!="body"&&FSUtil.visible(_e7)&&!YDOM.hasClass(_e7,"fsHiddenCell")?true:false;
if(!_e8){
return false;
}
var _e9=_e7.parentNode;
while(_e9&&_e9.tagName.toLowerCase()!="body"&&!YDOM.hasClass(_e9,"fsSection")){
_e9=_e9.parentNode;
}
if(!_e9||_e9.tagName.toLowerCase()=="body"){
return _e8;
}
return FSUtil.visible(_e9)&&!YDOM.hasClass(_e9,"fsHiddenCell");
};
this.checkForm=function(){
var res=this.checkRequired(this.lastPage);
if(res){
var _eb=[];
var _ec=YDOM.getElementsByClassName("fsRequired","","fsForm"+this.id);
for(var i=0;i<_ec.length;i++){
var _ee=_ec[i];
if(!this.fieldIsVisible(_ee)){
if(_ee.id.indexOf("_")>=0){
var m=_ee.id.split("_");
_eb.push(m[0]);
}else{
_eb.push(_ee.name);
}
}
}
if(YDOM.get("hidden_fields"+this.id)){
YDOM.get("hidden_fields"+this.id).value=_eb.join(",");
}
if(YDOM.get("captcha"+this.id)){
if(YDOM.get("captcha_code_"+this.id).value==""){
this.captchaError();
return false;
}
}
return true;
}else{
return false;
}
};
this.submitForm=function(){
if(!this.checkForm()){
return;
}
if(YDOM.get("captcha"+this.id)){
YDOM.get("fsSubmitButton"+this.id).disabled=true;
var _f0=YDOM.get("fsForm"+this.id).action.replace(/index.php$/,"captcha.php");
this.scriptRequest(_f0+"?action=test&v=2&captcha_code="+YDOM.get("captcha_code_"+this.id).value+"&form="+this.id+"&fspublicsession="+YDOM.get("session_id"+this.id).value+"&r="+(new Date()).getTime());
}else{
YDOM.get("fsForm"+this.id).submit();
}
};
this.captchaError=function(){
YDOM.addClass("captcha"+this.id,"captchaError");
FSUtil.scrollTo("captcha"+this.id);
};
this.reloadCaptcha=function(_f1){
var _f2=YDOM.get("fsForm"+this.id).action.replace(/index.php$/,"captcha.php");
YDOM.get("captcha_image_"+this.id).src=_f2+"?fspublicsession="+_f1+"&r="+Math.random();
};
this.scriptRequest=function(req){
var _f4=document.getElementsByTagName("head");
if(!_f4.length){
YDOM.get("fsForm"+this.id).submit();
return;
}
_f4=_f4[0];
var _f5=document.createElement("script");
_f5.setAttribute("type","text/javascript");
_f5.setAttribute("charset","utf-8");
_f5.setAttribute("src",req);
_f5.setAttribute("id","scriptRequest"+this.scriptRequestCounter);
_f4.appendChild(_f5);
this.scriptRequestCounter++;
};
this.captchaTestCallback=function(_f6){
if(_f6.res=="OK"){
YDOM.get("fsForm"+this.id).submit();
}else{
this.captchaError();
}
YDOM.get("fsSubmitButton"+this.id).disabled=false;
};
this.calendarShow=function(_f7,_f8,_f9){
var _fa=YDOM.getRegion(_f9.containerId+"Link");
if(_fa){
YDOM.setStyle(_f9.oDomContainer,"top",_fa.top+"px");
YDOM.setStyle(_f9.oDomContainer,"left",(_fa.left+16)+"px");
}
var id=_f9.id.match(/(\d+)/);
id=id[1];
var cur=new Date;
var _fd=YDOM.get("field"+id+"M");
var _fe=_fd&&_fd.selectedIndex?_fd.selectedIndex:cur.getMonth()+1;
var _ff=YDOM.get("field"+id+"D");
var day=_ff&&_ff.selectedIndex?_ff.selectedIndex:cur.getDate();
var _101=YDOM.get("field"+id+"Y");
var year=cur.getFullYear();
if(_101&&_101.selectedIndex){
var year=parseInt(_101.options[_101.selectedIndex].value,10);
if(year<100){
year+=2000;
}
}
_f9.select(_fe+"/"+day+"/"+year);
_f9.setMonth(_fe-1);
_f9.setYear(year);
_f9.render();
};
this.calendarSelect=function(type,args,_105){
var id=_105.id.match(/(\d+)/);
id=id[1];
var _107=args[0];
var date=_107[0];
var year=date[0],_10a=date[1],day=date[2];
var _10c=YDOM.get("field"+id+"M");
if(_10c){
_10c.selectedIndex=_10a;
}
var _10d=YDOM.get("field"+id+"D");
if(_10d){
_10d.selectedIndex=day;
}
var _10e=YDOM.get("field"+id+"Y");
if(_10e){
for(var y=1;y<_10e.options.length;y++){
var _110=parseInt(_10e.options[y].value,10);
if(_110<100){
_110+=2000;
}
if(_110==year){
_10e.selectedIndex=y;
break;
}
}
}
_105.hide();
};
this.textareaCharLimiter=function(id,_112){
var _113=YDOM.get("field"+id);
var _114=YDOM.get("fsCounter"+id);
var text=YDOM.get(_113).value;
if(text.length>_112){
_113.value=text.substring(0,_112);
}
_114.innerHTML=_112-YDOM.get(_113).value.length;
var _116=YDOM.getRegion(_113.id);
if(_116){
YDOM.setStyle(_114.id,"top",(_116.bottom-FSUtil.getHeight(_114)-5)+"px");
YDOM.setStyle(_114.id,"left",(_116.right-FSUtil.getWidth(_114)-25)+"px");
}
};
this.getFieldsByName=function(name){
var _118=new Array();
var els=document.getElementsByName(name);
if(els.length>0){
for(var i=0;i<els.length;i++){
_118.push(els[i]);
}
}else{
var els=document.getElementsByName(name+"[]");
for(var i=0;i<els.length;i++){
_118.push(els[i]);
}
}
return _118;
};
this.saveIncomplete=function(){
if(!confirm(YDOM.get("resumeConfirm")?YDOM.get("resumeConfirm").innerHTML:"Are you sure you want to leave this form and resume later?")){
return;
}
YDOM.get("incomplete"+this.id).value="true";
YDOM.get("fsForm"+this.id).submit();
};
this.checkFreeLink=function(){
var form=YDOM.get("fsForm"+this.id);
if(!YDOM.hasClass(form,"fsFormFree")){
return true;
}
var doc;
var type=YDOM.get("referrer_type"+this.id);
switch(type.value){
case "iframe":
doc=window.parent.document;
break;
case "js":
doc=window.document;
break;
default:
return true;
}
var _11e=false;
var _11f=doc.getElementsByTagName("a");
for(var i=0;i<_11f.length;i++){
if((_11f[i].href.indexOf("http://www.formspring.com/")==0)||(_11f[i].href.indexOf("http://www.formstack.com/")==0)){
_11e=true;
break;
}
}
if(_11e){
return true;
}
this.showError(YDOM.get("embedError")?YDOM.get("embedError").innerHTML:"There was an error displaying the form. Please copy and paste the embed code again.");
FSUtil.hide(form);
return false;
};
this.checkMatrixOnePerColumn=function(id){
var ids=id.split("-");
var _123=ids[0];
var _124=ids[1];
var _125=ids[2];
var _126=YDOM.get("matrix-"+_123).getElementsByTagName("input");
for(var i=0;i<_126.length;i++){
var re=new RegExp("^"+_123+"-\\d+-"+_125+"$");
if(_126[i].id!=id&&re.test(_126[i].id)){
_126[i].checked=false;
}
}
};
this.getNumberProperties=function(_129){
var _12a={min:NaN,max:NaN,decimals:NaN};
var _12b=_129.className.split(/\s+/);
for(var i=0;i<_12b.length;i++){
var _12d=_12b[i];
var _12e;
if(_12e=_12d.match(/^fsNumberMin-([\-\d]+)/)){
_12a.min=parseInt(_12e[1]);
}else{
if(_12e=_12d.match(/^fsNumberMax-([\-\d]+)/)){
_12a.max=parseInt(_12e[1]);
}else{
if(_12e=_12d.match(/^fsNumberDecimals-([\d]+)/)){
_12a.decimals=parseInt(_12e[1]);
}
}
}
}
return _12a;
};
};
function FSUtil(){
};
FSUtil.checkAll=function(el){
var _130=el.name.replace("_all","_");
var i=1;
var e;
while(e=document.getElementById(_130+i)){
e.checked=el.checked;
i++;
}
};
FSUtil.show=function(el){
YDOM.setStyle(el,"display","");
};
FSUtil.hide=function(el){
YDOM.setStyle(el,"display","none");
};
FSUtil.visible=function(el){
return YDOM.getStyle(el,"display")!="none";
};
FSUtil.scrollTo=function(el){
window.scroll(YDOM.getX(el),YDOM.getY(el));
};
FSUtil.getHeight=function(el){
var _138=YDOM.getRegion(el);
var _139=_138.bottom-_138.top;
return isNaN(_139)?0:_139;
};
FSUtil.getWidth=function(el){
var _13b=YDOM.getRegion(el);
var _13c=_13b.right-_13b.left;
return isNaN(_13c)?0:_13c;
};
FSUtil.arrayIndexOf=function(arr,item){
for(var i=0;i<arr.length;i++){
if(arr[i]===item){
return i;
}
}
return -1;
};

